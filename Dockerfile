FROM debian:buster AS build

RUN apt-get update && apt-get install -y git cmake perl yasm g++ python3 zlib1g-dev libjpeg-dev libpng-dev

RUN mkdir /tmp/build; mkdir /tmp/install; \
    cd /tmp/build && \
    git clone --branch v3.1.1 --depth=1 https://aomedia.googlesource.com/aom && \
    mkdir /tmp/build/libaom_build

RUN cd /tmp/build/libaom_build; cmake ../aom -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_SHARED_LIBS=1 && make -j`nproc` && make install && make install DESTDIR=/tmp/install

# Cleanup build for libaom
RUN rm -rf /tmp/build/{libaom_build,aom}

# Fetch libavif
RUN cd /tmp/build && git clone --branch v0.9.2 --depth=1 https://github.com/AOMediaCodec/libavif.git && mkdir /tmp/build/libavif_build

RUN cd /tmp/build/libavif_build; cmake ../libavif -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_SHARED_LIBS=1 -DAVIF_CODEC_AOM=1 -DAVIF_BUILD_APPS=1 && make -j`nproc` && make install DESTDIR=/tmp/install

# Cleanup libavif build
RUN rm -rf /tmp/build/{libavif,libavif_build}

FROM debian:buster

RUN apt-get update && apt-get -y install libpng16-16 libjpeg62-turbo && rm -rf /var/lib/apt/lists/*

COPY --from=build /tmp/install/usr /usr
